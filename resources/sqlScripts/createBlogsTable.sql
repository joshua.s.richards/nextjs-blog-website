CREATE TABLE authors (
    authorId INT NOT NULL AUTO_INCREMENT,
    author varchar(256) NOT NULL,
    password varchar(256),
    PRIMARY KEY (authorId),
    UNIQUE(author)
);          

CREATE TABLE blogs (
    blogId INT NOT NULL AUTO_INCREMENT,
    authorId INT NOT NULL, 
    author varchar(256),
    timestamp BIGINT,
    title varchar(512),
    content TEXT,
    views INT,
    PRIMARY KEY (blogId),
    FOREIGN KEY (authorId) REFERENCES authors(authorId)
);

CREATE TABLE comments (
    blogId INT,
    authorId INT,
    author varchar(256),
    timestamp BIGINT,
    comment varchar(256),
    likes INT,
    FOREIGN KEY (blogId) REFERENCES blogs(blogId)
);

ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password';
