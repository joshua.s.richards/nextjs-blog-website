import React, { useState } from "react";
import { Comment } from "../pages/submit";

interface Props {
  comment: Comment;
  updateLikes: (likesNo: number) => void;
}

const CommentComponent: React.FC<Props> = ({ comment, updateLikes }) => {
  const [hasLiked, setHasLiked] = useState(false);
  const [hasDisliked, setHasDisliked] = useState(false);

  function increaseLikes() {
    if (!hasLiked) {
      updateLikes(comment.likes + 1);
      setHasLiked(true);
      setHasDisliked(false);
    }
    else {
      updateLikes(comment.likes - 1);
      setHasLiked(false);
      setHasDisliked(false);
    }
  }

  function decreaseLikes() {
    if (!hasDisliked) {
      updateLikes(comment.likes - 1);
      setHasDisliked(true);
      setHasLiked(false);
    }
    else {
      updateLikes(comment.likes + 1);
      setHasDisliked(false);
      setHasLiked(false);
    }
  }

  return (
    <div className="card center magic" id="card1">
      <span>{comment.author}</span>
      <p id="com1">{comment.content}</p>
      <span>{comment.likes} likes</span>
      <button onClick={increaseLikes}>like</button>
      <button onClick={decreaseLikes}>dislike</button>
    </div>
  );
}

export default CommentComponent;
