import React from "react";
import axios from "axios";
import {Comments} from "../pages/submit"
import Link from "next/link";

interface Props {
  blog: Blog;
  reloadPage?: () => void;
}

export interface Blog {
  author: string;
  authorId: string
  title: string;
  timestamp: number;
  content: string;
  views: number;
}

const BlogPost: React.FC<Props> = ({blog, reloadPage}) => {

function _deleteBlog(): void {
  axios.delete(`/blogs/${blog.title}`).then(()=>{
    reloadPage();
  });
}

    return (
        <div className="card magic" id="card1" style={{display: "block"}}>
      <Link href={`/blog/${blog.title}?authorId=${blog.authorId}`}>{blog.title}</Link>
      <p id="blog1">{blog.content}</p>
      <button onClick={_deleteBlog}>Delete</button>
    </div>
    );
}

export default BlogPost;
