import {useState,  useCallback, useEffect} from "react";
import axios from "axios";

export const useAuthorEndpoints = (authorId: string) => {
  const [author, setAuthor] = useState('');

  const getAuthor = useCallback(() => {
    axios
      .get(`/author/${authorId}`)
      .then(response => {
          console.log(response.data)
        setAuthor(response.data[0].author);
      })
      .catch(() => {
        setAuthor('');
      });
  }, [authorId]);

  useEffect(() => {
    if (authorId) {
      getAuthor();
    }
  }, [getAuthor]);

  return author;
};
