import React from "react";

const Footer: React.FC = () => {
    return (
        <div className="footer">
   <a href="https://www.facebook.com" className="fa fa-facebook"></a>
   <a href="https://plus.google.com/" className="fa fa-google"></a>
   <a href="https://uk.linkedin.com/" className="fa fa-linkedin"></a>
   <a href="https://www.instagram.com/?hl=en" className="fa fa-instagram"></a>
  </div>
    );
}

export default Footer;
