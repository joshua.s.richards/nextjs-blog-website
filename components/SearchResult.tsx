import React from "react";
import { Blog } from "./BlogPost";
import Link from "next/link";

interface Props {
    blog: Blog;
}

const SearchResult: React.FC<Props> = ({blog}) => {
    return (
        <div className="cardtwo" id="card1">
    <Link href={`/blog/${blog.title}?author=${blog.author}`}><a>{blog.title}</a></Link>
</div>
    );
}

export default SearchResult;
