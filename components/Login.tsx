import React, { useState } from "react";
import axios from "axios";

interface Props {
    isLoggedIn: boolean;
    loginFailedMessage: string;
    login: (username: string, password: string) => void;
    logout: () => void;
    register: (username: string, password: string) => void;
}

const Login: React.FC<Props> = ({isLoggedIn, loginFailedMessage, login, logout, register}) => {
    const [username, setUsername] = useState<string>('');
    const [password, setPassword] = useState<string>('');

    return (
        !isLoggedIn ? <div>
            <input placeholder="Username" onChange={e => setUsername(e.target.value)}/>
            <input placeholder="Password" onChange={e => setPassword(e.target.value)}/>
            <button onClick={() => login(username, password)}>Login</button>
            <button onClick={() => register(username, password)}>Register</button>
            {loginFailedMessage && <span>{loginFailedMessage}</span>}
        </div>
        : <button onClick={logout}>Logout</button>
    );
}

export default Login;
