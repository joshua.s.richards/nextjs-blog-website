import React, { useState, useEffect } from "react";
import Link from "next/link";
import Router from "next/router";
import axios from "axios";
import Login from "./Login";
import { useAuthorEndpoints } from "./hooks/useAuthorEndpoints";

interface Props {
  authorId: string;
}

const NavBar: React.FC<Props> = ({ authorId }) => {
  const [searchTerm, setSearchTerm] = useState<string>("");
  const [author, setAuthor] = useState<string>("");
  const [isLoggedIn, setIsLoggedIn] = useState<boolean>(false);
  const [loginFailedMessage, setLoginFailedMessage] = useState<string>("");
  const cookieAuthor = useAuthorEndpoints(authorId);

  useEffect(() => {
      console.log(cookieAuthor)
    if (cookieAuthor) {
      setAuthor(cookieAuthor);
      setIsLoggedIn(true);
    }
  }, [cookieAuthor]);

  function login(username: string, password: string) {
    axios
      .post("/login", { username, password })
      .then(() => {
        setAuthor(username);
        setIsLoggedIn(true);
        setLoginFailedMessage("");
      })
      .catch(() => setLoginFailedMessage("Username/Password is incorrect"));
  }

  const logout = (): void => {
      axios.post('/logout').then(() => {
        setIsLoggedIn(false);
        setAuthor('');
      });
  }

  function register(username: string, password: string) {
    axios
      .post("/register", { username, password })
      .then(() => {
        setAuthor(username);
        setIsLoggedIn(true);
        setLoginFailedMessage("");
      })
      .catch(() => setLoginFailedMessage("Username is already taken"));
  }

  return (
    <ul>
      <li>
        <Link href="/index">
          <a>Home</a>
        </Link>
      </li>
      {isLoggedIn && (
        <li>
          <a href="/submit">Create Blogs</a>
        </li>
      )}
      <li>
        <a href="/contact">Contact Us</a>
      </li>
      <li>
        <a href="/authors">Authors</a>
      </li>
      <div className="search">
        <button
          className="searchbutton"
          type="submit"
          style={{ fontFamily: "Courier New, Courier, monospace" }}
          onClick={() => Router.push(`/search/${searchTerm}`)}
        >
          Search
        </button>

        <input
          type="text"
          placeholder="Search for a blog..."
          name="search"
          onChange={e => setSearchTerm(e.target.value)}
        />
      </div>

      {isLoggedIn && <span className="author">{author}</span>}
      <Login
        login={login}
        logout={logout}
        register={register}
        isLoggedIn={isLoggedIn}
        loginFailedMessage={loginFailedMessage}
      />
    </ul>
  );
};

export default NavBar;
