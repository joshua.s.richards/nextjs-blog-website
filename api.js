const express = require("express");

const router = express.Router();

let mysql = require("mysql");
let connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "password",
  database: "blog"
});
connection.connect(function(err) {
  if (err) throw err;
  router.post("/blogs", (req, res) => {
    connection.query(
      `SELECT author from authors WHERE authorID = ${req.body.authorId}`,
      (err, author) => {
        if (err) throw err;
        connection.query(
          `INSERT INTO blogs (author, authorId, timestamp, title, content, views) VALUES ('${author}', ${
            req.body.authorId
          }, ${new Date().getTime()}, '${req.body.title}', '${
            req.body.content
          }', '${req.body.views}');`,
          function(err, result) {
            if (err) throw err;
            res.send(result);
          }
        );
      }
    );
  });

  router.get("/blogs", (req, res) => {
    connection.query("SELECT * from blogs;", (err, result) => {
      if (err) throw err;
      res.send(result);
    });
  });

  router.get("/blogs/:searchTerm", (req, res) => {
    connection.query(
      `SELECT * from blogs 
    WHERE title LIKE '%${req.params.searchTerm}%' OR 
    author LIKE '%${req.params.searchTerm}%' OR
    content LIKE '%${req.params.searchTerm}%';`,
      (err, result) => {
        if (err) throw err;
        res.send(result);
      }
    );
  });

  router.get("/blogs/blog/:title", (req, res) => {
    connection.query(
      `SELECT * from blogs 
    WHERE title = '${req.params.title}' AND 
    authorId = '${req.query.authorId}';`,
      (err, result) => {
        if (err) throw err;
        res.send(result);
      }
    );
  });

  router.post("/blogs/comments", (req, res) => {
    connection.query(
      `UPDATE comments SET authorId = ${req.body.authorId}, comments = ;`,
      (err, result) => {
        if (err) throw err;
        res.send(result);
      }
    );
  });

  router.delete("/blogs/:blogTitle", (req, res) => {
    connection.query(
      `DELETE FROM blogs WHERE blogs.title = '${req.params.blogTitle}';`,
      (err, result) => {
        if (err) throw err;
        res.send(result);
      }
    );
  });

  router.get("/author/:authorId", (req, res) => {
    connection.query(
      `SELECT author from authors WHERE authors.authorId=${req.params.authorId};`,
      (err, result) => {
        if (err) throw err;
        res.send(result);
      }
    );
  });

  router.post("/login", (req, res) => {
    connection.query(
      `SELECT authorId FROM authors WHERE authors.author = '${req.body.username}' AND authors.password = '${req.body.password}';`,
      (err, result) => {
        if (err) throw err;
        res.setHeader("Set-Cookie", [
          `authorId=${result[0].authorId}`,
          "language=javascript"
        ]);
        res.send(result);
      }
    );
  });

  router.post("/logout", (_, res) => {
    res.setHeader("Set-Cookie", [
      `authorId=deleted; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT`,
      "language=javascript"
    ]);
    res.send();
  });

  router.post("/register", (req, res) => {
    connection.query(
      `INSERT INTO authors (author, password) VALUES ('${req.body.username}', '${req.body.password}');`,
      (err, result) => {
        if (err) throw err;
        connection.query(
          `SELECT authorID FROM authors WHERE authors.author = '${req.body.username}' AND authors.password = '${req.body.password}';`,
          (err, result) => {
            if (err) throw err;
            res.setHeader("Set-Cookie", [
              `authorId=${result[0].authorId}`,
              "language=javascript"
            ]);
            res.send(result);
          }
        );
      }
    );
  });
});

module.exports = router;
