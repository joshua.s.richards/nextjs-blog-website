import React, { useState, useEffect } from "react";
import { GetServerSideProps } from 'next'
import Cookies from "cookies";
import BlogPost, { Blog } from "../components/BlogPost";
import NavBar from "../components/NavBar";
import Footer from "../components/Footer";
import Link from "next/link";
import axios from "axios";

interface Props {
  authorId: string;
}

const Index: React.FC<Props> = ({authorId}) => {
const [blogs, setBlogs] = useState<Blog[]>([]);

useEffect(() => {
        _loadBlogs();
}, []);

function _loadBlogs(): void {
    axios.get('/blogs').then(response => {
        setBlogs(response.data);
    });
}

    return (
        <html>
<link href="../static/style.css"  rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
<div className="header">
  <h2>The Blog-Celerator</h2>
</div>

<NavBar authorId={authorId}/>

<div className="row">
  <div className="leftcolumn">
      {blogs.map(blog=><BlogPost blog={blog} reloadPage={_loadBlogs}/>)}
    <div>
      <button type="button" style={{display:"none"}} id='back'>Back</button>
      <button type="button" style={{display:"none"}} id='next'>Next</button>
    </div>
  </div>
  <div className="rightcolumn">
    <div className="card">
      <div className="subheader">
       <h3>Create A Blog!</h3>
      </div>
<p>Click <Link href="/submit"><a>here</a></Link> to go and create your own blog! </p>
    </div>
    <div className="card">
       <div className="subheader">
        <h3>Most Viewed Blogs!</h3>
       </div>
      <div className="fakeimg">Image</div><br/>
      <div className="fakeimg">Image</div><br/>
      <div className="fakeimg">Image</div>
    </div>
    <div className="card">
      <div className="subheader">
       <h3>Contact Us:</h3>
      </div>
        <p></p>
        <p> For more information on our blogs or any of the services provided find us on social media or contact one of the team at:</p>
    </div>
  </div>
</div>

<Footer/>
  </html>
    )
}

export default Index;

export async function getServerSideProps({req, res}): GetServerSideProps {
  const cookies = new Cookies(req, res);

  return {
    props: {
      authorId: cookies.get('authorId') || null,
    },
  };
}
