import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import Cookies from "cookies";
import SearchResult from "../../../components/SearchResult";
import NavBar from "../../../components/NavBar";
import Footer from "../../../components/Footer";
import axios from "axios";
import { Blog } from "../../../components/BlogPost";

interface Props {
  authorId: string;
}

const Search: React.FC<Props> = ({authorId}) => {
  const [blogs, setBlogs] = useState<Blog[]>([]);
  const router = useRouter();

  useEffect(() => {
    const {searchTerm} = router.query;
    axios.get(`/blogs/${searchTerm}`).then(response => {
      setBlogs(response.data);
    });
  }, [router.query]);

    return (
        <html>
<link href="/static/style.css"  rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
<div>
<h2>Search Results</h2>
</div>

<NavBar authorId={authorId}/>

<div className="cardtwo">
  <h2 className="subheader" style={{width:"95%"}}>Blogs matching your search</h2>
  {blogs.map(blog=><SearchResult blog={blog}/>)}
</div>

<Footer/>
  </html>
    );
}

export default Search;

export async function getServerSideProps({req, res}) {
  const cookies = new Cookies(req, res);

  return {
    props: {
      authorId: cookies.get('authorId'),
    },
  };
}
