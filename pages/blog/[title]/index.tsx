import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import axios from "axios";
import Cookies from "cookies";
import NavBar from "../../../components/NavBar";
import Footer from "../../../components/Footer";
import { Blog } from "../../../components/BlogPost";
import CommentComponent from "../../../components/Comment";
import { Comment } from "../../submit";

interface Props {
  authorId: string;
}

const BlogPage: React.FC<Props> = ({authorId}) => {
  const [blog, setBlog] = useState<Blog>(null);
  const [comments]
  const router = useRouter();

  function loadPage(title: string, author: string) {
    axios.get(`/blogs/blog/${title}?authorId=${authorId}`).then(response => {
      setBlog(response.data[0]);
    });
  }

  useEffect(() => {
    const { title, authorId } = router.query;
    loadPage(title as string, authorId as string);
  }, [router.query]);

  function updateLikes(comment: Comment): (likesNo: number) => void {
    return (likesNo: number) => {
      const { title, author } = router.query;
      const newComment = JSON.parse(blog.comments).comments.map(originalComment =>
        originalComment.author === comment.author && originalComment.content === comment.content ?
          { author: originalComment.author, content: originalComment.content, likes: likesNo } :
          originalComment)
      axios.post(`/blogs/comments?author=${author}&title=${title}`, newComment).then(() => {
        loadPage(title as string, author as string);
      });
    }
  }

  function addComment(content: string) {
    const { title, author } = router.query;
    const newComment = { comments: [...JSON.parse(blog.comments).comments, { content, likes: 0, author: 'test' }] };
    axios.post(`/blogs/comments?author=${author}&title=${title}`, newComment).then(() => {
      loadPage(title as string, author as string);
    });
  }

  return (
    <html>
      <link href="/static/style.css" rel="stylesheet" type="text/css" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
      <div>
        <h2 className="header2">Individual Blogs & Comments</h2>
      </div>


      <NavBar authorId={authorId}/>

      <div className="card">
        <div className="subheader">
          <h2 id="Author">Author</h2>
          <span>{blog && blog.author}</span>
        </div>
        <h3 id="Title">Title</h3>
        <span>{blog && blog.title}</span>
        <h3 id="Date">Date</h3>
        <span>{blog && new Date(blog.timestamp).getDate}</span>
        <p style={{ textAlign: "justify" }} id="blog"></p>
      </div>
      <div className="cardtwo">
        <textarea name="comment" id="comment" style={{ resize: "none", fontSize: "16px" }} rows={8} cols={120} placeholder="Enter your comments here and please be nice." />
        <input type="submit" onClick={() => addComment((document.getElementById('comment') as HTMLTextAreaElement).value)} />
        {blog && JSON.parse(blog.comments).comments.map(comment => <CommentComponent comment={comment} updateLikes={updateLikes(comment)} />)}
      </div>
      <div>
        <button type="button" onClick={() => { }} style={{ display: "none" }} id='back'>Back</button>
        <button type="button" onClick={() => { }} style={{ display: "none" }} id='next'>Next</button>
      </div>
      <Footer />
    </html>
  );
}

export default BlogPage;

export async function getServerSideProps({req, res}) {
  const cookies = new Cookies(req, res);

  return {
    props: {
      authorId: cookies.get('authorId'),
    },
  };
}
