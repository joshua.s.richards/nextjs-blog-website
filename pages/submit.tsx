import React, { useState } from "react";
import Cookies from "cookies"
import Footer from "../components/Footer";
import NavBar from "../components/NavBar";
import axios from "axios";
import Router from "next/router";

interface Props {
  authorId: string;
}

interface BlogRequest {
  authorId: string;
  title: string;
  content: string;
  comments: Comments;
  views: number;
}

export interface Comments {
  comments: string;
}

export interface Comment {
  content: string;
  author: string;
  likes: number;
}

const Submission: React.FC<Props> = ({authorId}) => {
const [title, setTitle] = useState('');
const [content, setContent] = useState('');
function _saveBlog() {
  console.log(authorId);
  const blogRequest: BlogRequest = {
    authorId: authorId,
    title: title,
    content: content,
    comments: {comments: JSON.stringify({comments: []})},
    views: 0,
  };

  axios.post('/blogs', blogRequest).then(()=>{
    Router.push('/');
  });
}

    return (
        <html>
<link href="/static/style.css"  rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
<div>
<a className="header2"  href="{{ url_for('home') }}"><h2>Creating Your Blog</h2></a>
</div>

<NavBar authorId={authorId}/>

<div className="row">
  <div className="leftcolumn">
    <div className="cardtwo">

      <label className="subheader">Blog Title:</label>

      <input type="text" name="title" id="title" style={{fontSize:"16px"}} onChange={e=>setTitle(e.target.value)}/>


    <div className="cardtwo">
      <textarea id="blog" style={{resize:"none", fontSize:"16px"}} name="text" rows={30} cols={85} onChange={e=>setContent(e.target.value)} placeholder="Blow us away with your incredible blog making skills here!"/>
      <input type="submit" onClick={_saveBlog}/>
    </div>
    </div>
  </div>
</div>
<Footer/>
  </html>
    )
}

export default Submission;

export async function getServerSideProps({req, res}) {
  const cookies = new Cookies(req, res);

  return {
    props: {
      authorId: cookies.get('authorId'),
    },
  };
}
